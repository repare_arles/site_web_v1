const path = require('path')
const webpack = require('webpack')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')
const { ProvidePlugin } = require('webpack')

module.exports = {
    entry: {
        main: path.resolve(__dirname, './src/index.js'),
        /*css: path.resolve(__dirname, './src/stylesheets/style.css'),*/
    },
    output: {
        path: path.resolve(__dirname, './dist'),
        filename: '[name].bundle.js',
    },
    module: {
        rules: [
            {
                test: /\.m?js$/,
                exclude: /(node_modules|bower_components)/,
                use: 'babel-loader',
            },
            {
                test: /\.(ico|png|jpg|jpeg|gif|svg|webp)$/i,
                type: 'asset/resource',
                generator: {
                    filename: 'assets/images/[hash][ext][query]'
                }
            },
            {
                test: /\.(otf|eot|svg|ttf|woff|woff2)$/,
                type: 'asset/resource',
                generator: {
                    filename: 'assets/fonts/[hash][ext][query]'
                }
            },
            {
                test: /\.(mustache)$/,
                type: 'asset/source'
            },
            {
                test: /\.(md)$/,
                use: 'raw-loader'
            },
            {
                test: /\.(png|woff|woff2|eot|ttf|svg)$/,
                type: "asset/resource",
            }
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: "./src/index.html",
            title: 'Repare Arles',
            /*favicon: path.resolve(__dirname, './src/images/favicon.ico')*/
        }),
        new HtmlWebpackPlugin({
            template: "./src/mentions-legales.html",
            filename: "mentions-legales.html",
            title: 'Mentions légales',
        }),
        new CleanWebpackPlugin(),
        new webpack.HotModuleReplacementPlugin(),
    ]
}
